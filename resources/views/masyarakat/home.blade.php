@extends('masyarakat.base')
@section('content')
<style media="screen">
  tr{
    background-color: #000000;
    color: white;
  }
  td{
    background-color: #000000;
    padding: 10px;
  }
</style>
    <!-- Main Section -->
    <section class="main-section">
        <!-- Add Your Content Inside -->
        <div class="content">
            <!-- Remove This Before You Start -->
            <!-- <h1>PAGE MASYARAKAT </h1>
            <p>Mau lapor apa hari ini?, {{Session::get('')}}.</p> -->
            <div>
              <a style=" background-color: #000000;padding:10px;color:white;"href="{{ url('logout') }}" class="btn-logout">Logout</a>
              <h1>
                <p style="text-align:center">
                  PENGADUAN MASYARAKAT
                </p>
              </h1>
              <p style="">Hallo! {{Session::get('username')}}</p>
            </div>

            <div class="adddata" style="background-color: #000000;">
              <a style="color:white; text-decoration:none; font-size:30px;"
               href="{{url('masyarakat/pengaduan/tambah')}}">Buat Laporan</a>
            </div>
            <div>
              <br>
              <br>
              <br>
              <br>
              <br>
              <h5>
                <p style="text-align:center">
                  Laporan yang telah dibuat
                </p>
              </h5>
            </div>
            <table class="table">
                <tr>
                    <th>Tanggal</th>
                    <th>Isi Laporan</th>
                    <th>Foto</th>
                    <th>Aksi</th>
                </tr>
                    @foreach($pengaduan as $data)
                <tr>
                    <td>{{$data->tgl_pengaduan}}</td>
                    <td>{{$data->isi_laporan}}</td>
                    <td><a style="background-color: #f44336;padding: 5px;text-decoration:none;color:white;"target="_blank" href="url('../../../../public/images/{{$data->foto}}">Show</a></td>
                    <td>
                        <a style="background-color: #f44336;padding: 10px;float: left;text-decoration: none;color:white;
                        "href="{{url('masyarakat/pengaduan/edit/'.$data->id_pengaduan)}}">Edit</a>

                    <form action="{{ url('masyarakat/pengaduan/hapus'.$data->id_pengaduan) }}" method="post">
                      @method('DELETE')
                      @csrf
                      <button style="background-color:#f44336;padding: 10px;float: right;font-family:'Nexa-Regular';color: white;
                      border:none;"type="submit">Delete</button>
                    </form></td>
                </tr>
                    @endforeach
            </table>

            <p style="text-align:center">
              *Jika laporan yang dibuat sudah tidak ditampilkan, maka sudah dikonfirmasi oleh Petugas.
            </p>
            <br>

            <p style="text-align:center">
              PEMAS
            </p>

        </div>
        <!-- /.content -->
    </section>
    <!-- /.main-section -->
@endsection
